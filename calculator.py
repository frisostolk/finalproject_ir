# Filename: calculator.py
# This program takes an csv file and calculates the gun per person rate 
# and gun crimes per person
import sys
import csv
import pylab as pl
import matplotlib.pyplot as plt
def main():
    # Making empty lists to fill
    csv_list = []
    land_list = []
    csv_file = sys.argv[1] # Setting a minimum of 1 file to run with this program.
    gun_kills = ""
    """ This opens the csv file and reads it per line"""
    with open(csv_file) as csvf:
        csv_reader = csv.reader(csvf, delimiter=',')
        for row in csv_reader:
            guns_per_person = int(row[2]) / int(row[1])
            gun_crimes_per_person = int(row[3]) / int(row[1])
            gun_kills = int(row[3])
            homicides = int(row[4])
            csv_list.append((guns_per_person, gun_crimes_per_person, gun_kills, homicides))
    guns = []
    crimes = []
    low_gun_pp = 0
    high_gun_pp = 0
    low_hom_pp = 0
    high_hom_pp = 0
    csv_list.sort()
    """ Calculates the gun per person ratio and the homcide withou a gun per person ratio """
    for i in range(len(csv_list)):
        if i < len(csv_list)/2:
            low_gun_pp = low_gun_pp + csv_list[i][2]
            low_hom_pp = low_hom_pp + csv_list[i][3]
        if i > len(csv_list)/2:
            high_gun_pp = high_gun_pp + csv_list[i][2]
            high_hom_pp = high_hom_pp + csv_list[i][3]
    low_hom_pp = low_hom_pp - low_gun_pp
    high_hom_pp = high_hom_pp - high_gun_pp
    print("Low gun per person ration gun kills: " + str(low_gun_pp))
    print("High gun per person ratio gun kills: " + str(high_gun_pp))
    print("Low gun per person ratio homicides without gun: " + str(low_hom_pp))
    print("High gun per person ratio homicides without gun: " + str(high_hom_pp))
    small_guns = 0
    big_guns = 0
    average = []
    """ Calculates the average gun kills for the 7 lowest and the 7 highest countries"""
    for i in range(len(csv_list)):
        if i < len(csv_list)/2:
            small_guns = small_guns + csv_list[i][1]
        if i > len(csv_list)/2:
            big_guns = big_guns + csv_list[i][1]
    print(big_guns / (len(csv_list)) / 2)
    average.append(small_guns / (len(csv_list)) / 2)
    average.append(big_guns / (len(csv_list)) / 2)
    for i in range(len(csv_list)):
        guns.append(csv_list[i][0])
        crimes.append(csv_list[i][1])
    """ Setting a legenda and plots the figure"""
    plt.xlabel('Guns per person')
    plt.ylabel('Gun deaths per person')
    plt.title('Graph guns per person vs gun deaths')
    plt.plot(guns, crimes)
    plt.show()
    fig2 = pl.figure()
    ax2 = pl.subplot(111)
    ax2.bar(range(2), average) 
    pl.show()
main()
